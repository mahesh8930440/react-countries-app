import Header from "./components/Header";
import Main from "./components/Main";
import Country from "./components/Country";
import { createBrowserRouter, Outlet } from "react-router-dom";
const App = () => {
  return (
    <>
      <Header />
      <Outlet />
    </>
  );
};

const appRouter = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: "/",
        element: <Main />,
      },
      {
        path: "/country/:id",
        element: <Country />,
      },
    ],
  },
]);

export default appRouter;
