import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import appRouter from "./App";
import { RouterProvider } from "react-router-dom";
import { ThemeProvider } from "./components/context";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <ThemeProvider>
      <RouterProvider router={appRouter} />
    </ThemeProvider>
  </React.StrictMode>
);
