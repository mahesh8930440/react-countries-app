import CountriesDetails from "../components/CountriesDetails";

const CardContainer = ({ filteredListOfCountries }) => {
  return (
    <div className="flex flex-wrap basis-1/2 md:basis-1/3 lg:basis-1/3 justify-between my-8">
      {filteredListOfCountries.map((country) => (
        <CountriesDetails countries={country} />
      ))}
    </div>
  );
};

export default CardContainer;
