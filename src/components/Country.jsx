import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { useContext } from "react";
import { ThemeContext } from "../components/context";

const Country = () => {
  const { theme } = useContext(ThemeContext);
  const { id } = useParams();
  const [countryDetails, setCountryDetails] = useState({});
  console.log(id);
  useEffect(() => {
    fetchingCountries();
  }, []);
  const fetchingCountries = async () => {
    try {
      const response = await fetch("https://restcountries.com/v3.1/all");
      const countries = await response.json();

      const requiredCountry = countries.find((country) => {
        if (country.cca3 === id) {
          return country;
        }
      });
      setCountryDetails(requiredCountry);
    } catch (err) {
      console.log(err);
    }
  };
  if (!countryDetails) {
    return <></>;
  } else {
    const {
      name,
      flags,
      region,
      subregion,
      capital,
      population,
      borders,
      tld,
    } = countryDetails;
    const language = countryDetails.languages
      ? Object.values(countryDetails?.languages).join(", ")
      : "";
    console.log(language);
    return (
      <div
        className={`p-20 py-5 ${
          theme ? "bg-slate-700 text-[#fff]" : "bg-light-mode text-stone-950"
        }`}
      >
        <Link to="/">
          <button
            className={`rounded-md border-solid p-4 shadow-xl ${
              theme
                ? "bg-slate-700 text-[#fff]"
                : "bg-light-mode text-stone-950"
            }  `}
          >
          <i class="fa-solid fa-arrow-left mr-2"></i>Back
          </button>
        </Link>
        <div className="flex justify-between mt-20 align-middle">
          {flags && (
            <img
              src={flags.svg}
              alt="country-flag"
              className="w-1/2 h-100 mr-7"
            />
          )}
          {name && (
            <div className="w-1/2 self-center mr-2">
              <h1 className=" text-4xl mb-8">{name.common}</h1>
              <h2>
                Native Name:
                <span className="ml-3 text-slate-600">
                  {name?.nativeName?.fra?.official}
                </span>
              </h2>
              <p>
                Population:
                <span className="ml-3 text-slate-600">{population}</span>
              </p>
              <p>
                Region:<span className="ml-3 text-slate-600">{region}</span>
              </p>
              <p>
                Subregion:
                <span className="ml-3 text-slate-600">{subregion}</span>
              </p>
              <p>
                Capital:<span className="ml-3 text-slate-600">{capital}</span>
              </p>
            </div>
          )}
          <div className="w-1/2 self-center ml-2">
            <p className="mt-18.5 ">
              Top Level Domain:
              <span className="ml-3 text-slate-600">{tld}</span>
            </p>
            <p>
              Languages:<span className="ml-3 text-slate-600">{language}</span>
            </p>
          </div>
        </div>

        <div>
          {borders && borders.length > 0 ? (
            <div>
              <p className="flex flex-wrap justify-center my-10">
                <span className=" mr-4 self-center">Boundaries:</span>
                {borders.map((country, index) => (
                  <span
                    key={index}
                    className={`rounded-md border-solid shadow p-4 ${
                      theme
                        ? "bg-slate-700 text-[#fff]"
                        : "bg-light-mode text-stone-950"
                    }  mr-4 my-4`}
                  >
                    {country}
                  </span>
                ))}
              </p>
            </div>
          ) : (
            <p>Boundaries: None</p>
          )}
        </div>
      </div>
    );
  }
};

export default Country;
