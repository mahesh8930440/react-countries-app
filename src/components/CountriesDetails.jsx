import { Link } from "react-router-dom";
const CountriesDetails = ({ countries }) => {
  const { name, flags, region, capital, population } = countries;
  return (
    <Link key={countries.cca3} to={`/country/${countries.cca3}`}>
      <div className="w-60 w-lg-100 h-[440px] rounded shadow-xl mt-9">
        <img
          src={flags.png}
          alt={name.common}
          className="h-1/2 w-full rounded"
        />
        <div className="pt-2 pl-4">
          <h2 className="mt-6 text-[1.5rem]">{name.common}</h2>
          <p>Population: {population}</p>
          <p>Region: {region}</p>
          <p>Capital: {capital}</p>
        </div>
      </div>
    </Link>
  );
};
export default CountriesDetails;
