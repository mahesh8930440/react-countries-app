import { useContext } from "react";
import { ThemeContext } from "../components/context";

const Header = () => {
  const { theme, toggleTheme } = useContext(ThemeContext);
  return (
    <div
      className={`flex justify-between p-20 py-5 text-center ${
        theme ? "bg-slate-700 text-[#fff]" : "bg-light-mode text-stone-950"
      }`}
    >
      <h1 className="text-2xl">Where in the world?</h1>
      <button onClick={toggleTheme}><i class="fa-regular fa-moon mr-3"></i>Dark Mode</button>
    </div>
  );
};
export default Header;
