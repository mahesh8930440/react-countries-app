import CardContainer from "../components/CardContainer";
import { useEffect, useState } from "react";
import { useContext } from "react";
import { ThemeContext } from "../components/context";

const Main = () => {
  const { theme } = useContext(ThemeContext);
  const [listOfCountries, setListOfCountries] = useState([]);
  const [filteredListByRegion, setFilteredListByRegion] = useState([]);
  const [filteredSearchedListOfCountries, setFilteredSearchedListOfCountries] =
    useState([]);
  const [searchedText, setSearchedText] = useState("");
  const [filterItem, setFilterItem] = useState("all");

  const fetchingCountries = async () => {
    try {
      const response = await fetch("https://restcountries.com/v3.1/all");
      const countries = await response.json();
      console.log(countries);
      setListOfCountries(countries);
      setFilteredSearchedListOfCountries(countries);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchingCountries();
  }, []);

  function searchInput(e) {
    setSearchedText(e.target.value);

    const filteredCountries = e.target.value
      ? filteredListByRegion.filter((country) =>
          country.name.common
            .toLowerCase()
            .includes(e.target.value.toLowerCase())
        )
      : filteredListByRegion;

    setFilteredSearchedListOfCountries(filteredCountries);
  }

  function filterByRegion(e) {
    setFilterItem(e.target.value);

    const filterRegions =
      e.target.value === "all"
        ? listOfCountries
        : listOfCountries.filter(
            (country) => country.region === e.target.value
          );

    setFilteredListByRegion(filterRegions);
    setFilteredSearchedListOfCountries(filterRegions);

    console.log(filteredListByRegion);
  }

  return (
    <div
      className={`p-20 py-5 ${
        theme ? "bg-slate-700 text-[#fff]" : "bg-light-mode text-stone-950"
      }`}
    >
      <div className="flex justify-between">
        <input
          type="search"
          placeholder="Search for a country.."
          className={`rounded-md border-solid shadow-xl p-3 text-center ${
            theme ? "bg-slate-700 text-[#fff]" : "bg-light-mode text-stone-950"
          }`}
          value={searchedText}
          onChange={searchInput}
        />
        <select
          className={`rounded-md border-solid shadow-xl p-3  text-center ${
            theme ? "bg-slate-700 text-[#fff]" : "bg-light-mode text-stone-950"
          }`}
          value={filterItem}
          onChange={filterByRegion}
        >
          <option value="all" className="pr-2">
            Filter by Region
          </option>
          <option value="Africa" className="pr-2">
            Africa
          </option>
          <option value="Americas" className="pr-2">
            Americas
          </option>
          <option value="Asia" className="pr-2">
            Asia
          </option>
          <option value="Europe" className="pr-2">
            Europe
          </option>
          <option value="Oceania" className="pr-2">
            Oceania
          </option>
        </select>
      </div>
      <CardContainer
        filteredListOfCountries={filteredSearchedListOfCountries}
      />
    </div>
  );
};
export default Main;
