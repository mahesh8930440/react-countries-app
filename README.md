# Rest Countries Project

## Setup

- Clone the project:

- `git clone git@gitlab.com:mahesh8930440/react-countries-app.git`

- Navigate into the project directory:

- `cd react-countries-app`

- Install dependencies: `
  - npm install`

## Run the Project

- Run the project:
- `npm start`

## Features

- See all countries from the API on the homepage.
- Search for a country using an input field.
- Filter countries by region.
- Both the search and the filter feature work together.

## API Endpoint

- [https://restcountries.com/v3.1/all]
